/**
 * Classe contract of maintenance
 * @author grave - jeanine - antoine
 * @version 1.0
 * */
public class contrat_maintenance {
	/** 
	 * type of the contract
	 */
	private String type;
	/** 
	 * date of the signature
	 */
	private java.sql.Date datedebut; 
	/** 
	 * duration of the contract
	 */
	private java.sql.Date datefin;
	
	private String nomentreprise;

	/**
	 * Constructeur
	 * @param type of the contract
	 * @param date of the sign
	 * @param end of the contract
	 */
	public contrat_maintenance(String type, java.sql.Date datedebut, java.sql.Date datefin, String nomentreprise){
		this.type=type;
		this.datedebut=datedebut;
		this.datefin=datefin;
		this.nomentreprise=nomentreprise;
	}
	/**
	 * getter for the type
	 * @return value of type
	 */	
	public String getType(){
		return type;
	}
	/**
	 * getter for the date
	 * @return value of the date
	 */
	public java.sql.Date getDatedebut(){
		return datedebut;
	}
	/**
	 * getter pour la duree du contrat
	 * @return value of the date
	 */
	public java.sql.Date getDatefin(){
		return datefin;
	}
	/**
	 * getter for the type
	 * @return value of nomentreprise
	 */	
	public String getNomentreprise(){
		return nomentreprise;
	}
	/**
	 * setter  for login
	 * @param type :  new value for attribute login
	 */

	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "contrat :  l'entreprise : " + nomentreprise + " a signe un contrat pour des maintenances de type" + type+" sur une periode de : date du debut: " + datedebut
				+ "et date de fin : " + datefin +"";
	}
}
