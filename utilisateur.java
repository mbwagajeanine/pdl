/**
 * Classe User
 * @author grave - jeanine - antoine
 * @version 1.0
 * */
public class utilisateur {
	/** 
	 * login of the user
	 */
private String login;
/** 
 * password of the user
 */
private String mdp;
/** 
 * id of the user
 */
private int identifiant;
/** 
 * id of the user
 */
private profil Profil;

/**
 * Constructor
 * @param login of the user
 * @param password of the user
 * @param number or id of the user
 * @param profil de l'utilisateur
 */
public utilisateur( String login, String mdp, int identifiant, profil Profil ){
	this.login=login;
	this.mdp=mdp;
	this.identifiant=identifiant;
	this.Profil=Profil;
}

public utilisateur( String login, String mdp){
	this.login=login;
	this.mdp=mdp;
	
}
/**
 * getter for the login
 * @return value of login
 */
public String getLogin(){
	return login;
}
/**
 * getter for the attribute mdp
 * @return value of mdp
 */
public String getMdp( ) {
	return mdp;
}

/**
 * getter for attribute identifiant
 * @return value of identifiant
 */
public int getIdentifiant(){
	return identifiant;
}
/**
 * getter for the attribute mdp
 * @return value of mdp
 */
public profil getProfil() {
	return Profil;
}
/**
 * setter  for login
 * @param puHt :  new value for attribute login
 */
public void setLogin(String login){
	this.login=login;
	
}
/**
 * setter  for mdp
 * @param puHt :  new value for attribute mdp
 */
public void setMdp(String mdp){
	this.mdp=mdp;
}
/**
 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
 * pour l'affichage par exemple
 */
public String toString() {
	return "utilisateur [login : " + login + " - mot de passe:  " + mdp +
			 "avec pour fonction " + Profil.getFonction() + " est enregistr� ]";
}
}
