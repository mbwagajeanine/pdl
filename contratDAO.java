import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table contrat maitenance
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public class contratDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public contratDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un contrat a une liste de contrat dans la table contrat Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param contrat
	 *            le contrat à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(contrat_maintenance contrat_maintenance) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO CONTRAT (CR_TYPE, CR_DATEDEBUT, CR_DATEFIN, CR_NOMENTREPRISE, CR_ID) VALUES (?, ?, ?,?,seq.NEXTVAL)");
			ps.setString(1, contrat_maintenance.getType());
			ps.setDate(2,contrat_maintenance.getDatedebut() );
			ps.setDate(3, contrat_maintenance.getDatefin());
			ps.setString(4, contrat_maintenance.getNomentreprise());

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un article à partir de sa référence
	 * 
	 * @param reference
	 *            la référence de l'article à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun article ne correspond à cette référence
	 */
	public contrat_maintenance getcontrat_maintenance(String type) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		contrat_maintenance retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM CONTRAT WHERE CR_TYPE = ?");
			ps.setString(1, type);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new contrat_maintenance(rs.getString("CR_TYPE"),
						rs.getDate("CR_DATEDEBUT"),
						rs.getDate("CR_DATEFIN"), rs.getString("CR_NOMENTREPRISE"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les articles stockés dans la table article
	 * 
	 * @return une ArrayList d'Articles
	 */
	public List<contrat_maintenance> getListecontrat() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<contrat_maintenance> retour = new ArrayList<contrat_maintenance>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM CONTRAT");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new contrat_maintenance(rs.getString("CR_TYPE"),
						rs.getDate("CR_DATEDEBUT"),
						rs.getDate("CR_DATEFIN"), rs.getString("CR_NOMENTREPRISE")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		contratDAO contratDAO = new contratDAO();
		// test de la méthode ajouter
		contrat_maintenance a1 = new contrat_maintenance("palliative",Date.valueOf("2017-05-13"), Date.valueOf("2018-05-26"),"ESIGELEC");
		
		int retour = contratDAO.ajouter(a1);

		System.out.println(retour + " lignes ajoutées");

		// test de la méthode getArticle
		contrat_maintenance a2 = contratDAO.getcontrat_maintenance("palliative");
		System.out.println(a2);

		// test de la méthode getListeArticles
		List<contrat_maintenance> liste = contratDAO.getListecontrat();
		// affichage des articles
		for (contrat_maintenance CR : liste) {
			System.out.println(CR.toString());
		}

	}
}

