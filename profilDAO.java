
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table contrat maitenance
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public class profilDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public profilDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un contrat a une liste de contrat dans la table contrat Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param contrat
	 *            le contrat à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(profil profil) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO PROFIL (PR_FONCTION, PR_ID) VALUES (?,?)");
			ps.setString(1, profil.getFonction());
			ps.setInt(2, profil.getIdentifiant() );
			

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un article à partir de sa référence
	 * 
	 * @param reference
	 *            la référence de l'article à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun article ne correspond à cette référence
	 */
	public profil getprofil(String fonction) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		profil retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM PROFIL WHERE PR_FONCTION = ?");
			ps.setString(1, fonction);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new profil(rs.getString("PR_FONCTION"),
						rs.getInt("PR_IDENTIFIANT")
						);

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les articles stockés dans la table article
	 * 
	 * @return une ArrayList d'Articles
	 */
	public List<profil> getListeprofil() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<profil> retour = new ArrayList<profil>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM PROFIL");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new profil(rs.getString("PR_FONCTION"),
						rs.getInt("PR_ID")
						));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		profilDAO profilDAO = new profilDAO();
		// test de la méthode ajouter
		//profil a1 = new profil("agent",1);
		
		//int retour = profilDAO.ajouter(a1);

		//System.out.println(retour + " lignes ajoutées");

		

		// test de la méthode getListeArticles
		List<profil> liste = profilDAO.getListeprofil() ;
		// affichage des articles
		for (profil PR : liste) {
			System.out.println(PR.toString());
		}

	}
}


