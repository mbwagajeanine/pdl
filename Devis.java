/**
 * Classe devis
 * @author  jeanine - antoine
 * @version 1.0
 * */
public class Devis {
	/** 
	 * name of the enterprise;
	 */
	private int identifiant;
	/** 
	 * SIRET number
	 */
	private Double prixmateriaux;
	/** 
	 * customer adress
	 */
	private Double prixmaintenance;
	/** 
	 * customer APE cod 
	 */
	private maintenance maintenance;
	
	
	/**
	 * Constructor
	 * @param adresse du client
	 * @param code APE du client
	 * @param identifiant du client
	 * @param numero du siret du client
	 * @param numero de siret
	 * @param telephone du client
	 */
	public Devis (int identifiant, double prixmateriaux, double prixmaintenance, maintenance maintenance){
		this.identifiant=identifiant;
		this.maintenance=maintenance;
		this.prixmaintenance=prixmaintenance;
		this.prixmateriaux=prixmateriaux;
		this.maintenance=maintenance;
	}
	
	
	/**
	 * getter pour l'adresse du client
	 * @return valeur de l'adresse
	 */
	public int getidentifiant(){
		return identifiant;
	}
	/**
	 * getter pour le code APE du client
	 * @return valeur du code
	 */
	public Double getprixmaintenance(){
		return prixmaintenance;
	}
	/**
	 * getter pour l''identifiant du client
	 * @return valeur de l'id
	 */
	public Double getprixmateriaux(){
		return prixmateriaux;
	}
	/**
	 * getter pour le nom de l'enreprise
	 * @return valeur du nom de l'entreprise
	 */
	public maintenance getmaintenance(){
		return maintenance;
	}
	

	
	/**
	 * setter pour le numero de telephone
	 * @param idMain :  une nouvelle valeur est attribu� au numero de telephone
	 */
	public void setidentifiant(int identifant){
		this.identifiant=identifiant;
	}
	/**
	 * setter pour le nom de l'entreprise
	 * @param nomentreprise : valeur du nom de l'entreprise 
	 */
	public void setprixmaintenance(Double  prixmaintenance){
		this.prixmaintenance=prixmaintenance;
	}
	/**
	 * setter pour l'adresse
	 * @param adresse :  une nouvelle valeur est attribu� a l'adresse
	 */
	public void setprixmateriaux(Double prixmateriaux){
		this.prixmateriaux=prixmateriaux;
	}
	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage du client
	 */
	public String toString() {
		return "Devis [la maintenance : " + maintenance.toString()+ " - est evalue a : " + prixmaintenance
				+ " avec des materiaux qui coutent : " + prixmateriaux + "]";
	}
	
}

