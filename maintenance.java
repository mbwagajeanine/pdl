/**
 * Classe maintenance
 * @author grave - jeanine - antoine
 * @version 1.0
 * */
public class maintenance {
	/** 
	 * numero d'une maintenance
	 */
	private int idMain;
	/** 
	 * type de maintenance
	 */
	private String typeMaintenance;
	/** 
	 * date de debut d'une maintenance
	 */
	private java.sql.Date datedebut;
	/** 
	 * date de fin d'une maintenance
	 */
	private java.sql.Date datefin;
	/** 
	 * statut d'une maintenance
	 */
	private String statut;
	/** 
	 * 
	 */
	private client Client;
	/** 
	 * 
	 */
	private operateur operateur;
	
	/**
	 * Constructor
	 * @param type de maintenance
	 * @param identifiant d'une maintenance
	 * @param date de fin de la maintenance
	 * @param statut d'une maintenance
	 * @param date de debut de la maintenance
	 * @param client 
	 * @param operateur
	 */
	public maintenance(String statut, java.sql.Date datedebut, String typeMaintenance, int idMain,java.sql.Date datefin,client Client){
		this.idMain=idMain;
		this.typeMaintenance=typeMaintenance;
		this.datedebut=datedebut;
		this.datefin=datefin;
		this.statut=statut;
		this.Client=Client;
	
		
	}
	public maintenance(String statut, java.sql.Date datedebut, String typeMaintenance, java.sql.Date datefin,client Client){
		
		this.typeMaintenance=typeMaintenance;
		this.datedebut=datedebut;
		this.datefin=datefin;
		this.statut=statut;
		this.Client=Client;
	
	}		
	
	public maintenance(String statut, java.sql.Date datedebut, String typeMaintenance, int idMain,java.sql.Date datefin, client Client, operateur operateur){
		this.idMain=idMain;
		this.typeMaintenance=typeMaintenance;
		this.datedebut=datedebut;
		this.datefin=datefin;
		this.statut=statut;
		this.Client=Client;
		this.operateur=operateur;
		
	}
	
	/**
	 * getter pour le type de maintenace
	 * @return valeur du type de maintenance
	 */
	public String gettypeMaintenance(){
		return typeMaintenance;
	}
	/**
	 * getter pour l'identifiant d'une maintenance
	 * @return valeur de d'identifiant
	 */
	public int getidMain(){
		return idMain;
	}
	/**
	 * getter pour la date de debut
	 * @return valeur de la date de debut
	 */
	public java.sql.Date getdatedebut(){
		return datedebut;
	}
	/**
	 * getter pour la valeur de l'identifiant
	 * @return valeur de l'identifiant
	 */
	public java.sql.Date getDatefin(){
		return datefin;
	}
	/**
	 * getter pour le statut de la maintenance
	 * @return valeur du statut d'une maintenance
	 */
	public String getStatut(){
		return statut;
	}
	/**
	 * getter pour 
	 * @return 
	 */
	public client getClient(){
		return Client;
	}
	/**
	 * getter pour le statut de la maintenance
	 * @return valeur du statut d'une maintenance
	 */
	public operateur getoperateur(){
		return operateur;
	}
	/**
	 * setter pour la fonction identifiant d'une maintenance
	 * @param idMain :  une nouvelle valeur est attribu� � l'identifiant
	 */
	public void setIdMain(int idMain){
		this.idMain=idMain;
	}
	
	/**
	 * setter pour la date de debut de la maintenance
	 * @param puHt :  une nouvelle valeur est attribu� � la date de debut
	 */
	public void setDatedebut(java.sql.Date datedebut){
		this.datedebut=datedebut;
	}
	/**
	 * setter pour la fonction date de fin
	 * @param puHt :  une nouvelle valeur est attribu� � la date de fin
	 */
	public void setDatefin(java.sql.Date datefin){
		this.datefin=datefin;
	}
	/**
	 * setter pour le statut de la maintenance
	 * @param puHt :  une nouvelle valeur est attribu� au client
	 */
	public void setStatut(String statut){
		this.statut=statut;
	}
	/**
	 * setter pour le type de la maintenance
	 * @param puHt :  une nouvelle valeur est attribu� au type
	 */
	public void setClient( client Client){
		this.Client=Client;
	}
	/**
	 * setter pour la fonction identifiant d'une maintenance
	 * @param idMain :  une nouvelle valeur est attribu� � l'identifiant
	 */
	public void setoperateur(operateur operateur){
		this.operateur=operateur;
	}

	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "maintenance de type : " + typeMaintenance + " debute le: " + datedebut
				+ "et a ete effectue : " + datefin + " statut "+statut+"est pour le client"+Client+"";
	}
	
	
	
	
	
}
