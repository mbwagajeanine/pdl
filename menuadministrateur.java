import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class menuadministrateur extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					menuadministrateur frame = new menuadministrateur();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public menuadministrateur() {
		setTitle("Gestion de Maintenances");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Chosir l'op\u00E9ration \u00E0 effectuer :");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel.setBounds(28, 11, 226, 37);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Saisir un client");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new clientfenetre();
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton.setBounds(127, 59, 218, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Valider un Contrat de maintenance");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_1.setBounds(87, 283, 334, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Modifier un devis");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_2.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_2.setBounds(127, 156, 218, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Valider une maintenance");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_3.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_3.setBounds(127, 190, 218, 23);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Consulter les reportings des maintenances");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				reportingGUI frame = new reportingGUI();
				frame.setVisible(true);
			}
		});
		btnNewButton_4.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_4.setBounds(87, 329, 334, 23);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("affecter un operateur \u00E0 une maintenance");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_5.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_5.setBounds(117, 239, 259, 23);
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Saisir un operateur");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new operateurfenetre();
			}
		});
		btnNewButton_6.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_6.setBounds(127, 88, 218, 23);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Saisir nouvel agent de clientele");
		btnNewButton_7.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_7.setBounds(124, 122, 221, 23);
		contentPane.add(btnNewButton_7);
		
		JButton btnSupprimerUneMaintenance = new JButton("Supprimer une maintenance");
		btnSupprimerUneMaintenance.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnSupprimerUneMaintenance.setBounds(87, 383, 334, 23);
		contentPane.add(btnSupprimerUneMaintenance);
		
		JButton btnNewButton_8 = new JButton("Supprimer un agent");
		btnNewButton_8.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_8.setBounds(87, 417, 141, 23);
		contentPane.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("Supprimer un operateur");
		btnNewButton_9.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton_9.setBounds(238, 417, 183, 23);
		contentPane.add(btnNewButton_9);
	}

}
