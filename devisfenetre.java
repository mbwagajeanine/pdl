
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;
import java.sql.*;


import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 * Classe clientfenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client via
 * la saisie du nom de l'entreprise,le numero de siret, le numero de telephone l'adresse 
 *    - Permet l'affichage de tous les clients dans la console
 * @author grave - jeanine-antoine
 * @version 1.3
 * */


public class devisfenetre extends JFrame implements ActionListener {
	
	 
	  JComboBox ComboBox1 = new JComboBox(); 
	  JComboBox ComboBox = new JComboBox(); 
	
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ArticleFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le 
	 */
	private JTextField textFieldidentifiant;

	/**
	 * zone de texte pour le
	 */
	private JTextField textFieldprixmaintenance;

	/**
	 * zone de texte pour 
	 * 
	 */
	private JTextField textFieldprixmateriaux;
	
	

	/**
	 * zone de texte pour le 
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour le 
	 */
	private JTextField textFieldtype;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldstatut;
	
	/**
	 * zone de texte pour 
	 */
	private JTextField textFielddatedebut;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFielddatefin;
	/**
	 * zone du client
	 */
	/**
	 * zone de texte pour le nom de l'entreprise
	 */
	private JTextField textFieldNomEntreprise;

	/**
	 * zone de texte pour le numero de siret
	 */
	private JTextField textFieldNumeroSiret;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldAdresse;
	/**
	 * zone de texte pour le numero de telephone
	 */
	private JTextField textFieldNumeroTelephone;
	/**
	 * zone de texte pour l'identifiant
	 */
	private JTextField textFieldIdentifiantclient;
	/**
	 * zone de texte pour le code ape
	 */
	private JTextField textFieldcodeApe;
	
	
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldNom;

	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldPrenom;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldAdresseoperateur;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldDateembauche;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFielddispo;
	/**
	 * zone de texte pour 
	 * 
	 */
	private JLabel labelidentifiant;

	/**
	 * label  numero de siret
	 */
	private JLabel labelprixmaintenance;
	/**
	 * label  identifiant
	 */
	private JLabel labelprixmateriaux;

	/**
	 * label adresse
	 */
	private JLabel labelidentifiantmaintenance;

	/**
	 * bouton d'ajout de la maintenance
	 */
	private JButton boutonvaliderdevis;
	/**
	 * bouton d'ajout de la maintenance
	 */
	private JButton boutonretour;

	/**
	 * bouton d'ajout du client
	 */
	private JLabel labeluser;
	

	/**
	 * instance de clientDAO permettant les acc�s � la base de donn�es
	 */
	private devisDAO devisDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public devisfenetre() {
		// on instancie la classe client DAO
		this.devisDAO = new devisDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("Nouvelle maintenance");
		// initialisation de la taille de la fen�tre
		this.setSize(300, 300);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.WHITE);

		// instantiation des composants graphiques
		labeluser = new JLabel("Entrez les informations sur la nouvelle maintenance");
		textFieldidentifiant = new JTextField();
		textFieldprixmaintenance = new JTextField();
		textFieldprixmateriaux = new JTextField();
		
		textFieldtype = new JTextField();
		textFieldstatut = new JTextField();
		textFieldIdentifiant= new JTextField();
		textFielddatedebut = new JTextField();
		textFielddatefin = new JTextField();
		textFieldNom = new JTextField();
		textFieldPrenom = new JTextField();
		textFieldAdresseoperateur = new JTextField();
		textFieldDateembauche = new JTextField();
		textFielddispo = new JTextField();
		
		textFieldNomEntreprise = new JTextField();
		textFieldNumeroSiret = new JTextField();
		textFieldAdresse = new JTextField();
		textFieldNumeroTelephone = new JTextField();
		textFieldcodeApe = new JTextField();
		//textFieldidentifiantmain = new JTextField();
	
		boutonvaliderdevis = new JButton(" Valider le Devis");
		//boutonAffichertouteslesmaintenances = new JButton(" Afficher toutes les maintenances");
		boutonretour= new JButton("Retour");
		//labelidentifiantmain = new JLabel("Entrer l'identitifant de la maintenance :");
		labelprixmaintenance = new JLabel("Prix de la maintenance :");
		
		labelprixmateriaux = new JLabel("Prix des materiaux pour la maintenance:");
		//labelDate_fin = new JLabel("date de fin de la demande de la maintenance:");
		//labelstatut = new JLabel("Statut :");
		//ComboBox.addItem("Non validee");
		 
		// zoneTextListmaintenance = new JTextArea(10, 20);
			//zoneDefilement = new JScrollPane(zoneTextListmaintenance);
			//zoneTextListmaintenance.setEditable(false);
		containerPanel.add(labeluser);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 10)));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		
		containerPanel.add(labelprixmaintenance);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldprixmaintenance);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		// ajout des composants sur le container
		
		containerPanel.add(labelprixmateriaux);
		// introduire une espace constant entre le label et le champ texte
		
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldprixmateriaux);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		/*containerPanel.add(labelDate_debut);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldDatedebut);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDate_fin);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldDatefin);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelstatut);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(ComboBox
				);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		*/
		

		containerPanel.add(boutonvaliderdevis);
		
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));

		/*containerPanel.add(boutonAffichertouteslesmaintenances);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 5)));
		containerPanel.add(zoneDefilement);
		containerPanel.add(Box.createRigidArea(new Dimension(20, 20)));

		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));
*/
		containerPanel.add(boutonretour);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonvaliderdevis.addActionListener(this);
		//boutonAffichertouteslesmaintenances.addActionListener(this);
		boutonretour.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe clientDAO
		 
		try {
			if (ae.getSource() == boutonvaliderdevis) {
				
				
				// on cr�e l'objet message
				Devis d = new Devis( 1,
						Double.parseDouble(this.textFieldprixmateriaux.getText()),
						Double.parseDouble(this.textFieldprixmaintenance.getText()),
						new maintenance(this.textFieldstatut.getText(),
								Date.valueOf(this.textFielddatedebut.getText()),
								this.textFieldtype.getText(), Integer.parseInt(this.textFieldIdentifiant.getText()),
								Date.valueOf(this.textFielddatefin.getText()),
								new client(
										this.textFieldNomEntreprise.getText(),
										this.textFieldNumeroSiret.getText(),
										this.textFieldAdresse.getText(),
										this.textFieldcodeApe.getText(),
										this.textFieldNumeroTelephone.getText()),
								new operateur(this.textFieldNom.getText(),
												this.textFieldPrenom.getText(),
												this.textFieldAdresse.getText(),
												Date.valueOf(this.textFieldDateembauche.getText()), Boolean.parseBoolean(this.textFielddispo.getText())
										)
								
						)
						
						);
			    
			
				// on demande � la classe de communication d'envoyer le client
				// dans la table CLIENT
				retour = devisDAO.ajouter(d);
				
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println( + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "le devis a �t� cr�e et enregistr� pour la maintenance s�lectionn�e !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout devis",
							"Erreur", JOptionPane.ERROR_MESSAGE);
				
			} 
			else if(ae.getSource()== boutonretour){
				
				new maintenancevalideefenetre();
				dispose();
			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler la saisie de vos donnees");
		}

	}

	public static void main(String[] args) {
		//dispose();
		new devisfenetre();
	}

}
