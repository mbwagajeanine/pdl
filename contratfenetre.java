
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;
import java.sql.*;


import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 * Classe clientfenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client via
 * la saisie du nom de l'entreprise,le numero de siret, le numero de telephone l'adresse 
 *    - Permet l'affichage de tous les clients dans la console
 * @author grave - jeanine-antoine
 * @version 1.3
 * */


public class contratfenetre extends JFrame implements ActionListener {
	
	 
	  JComboBox ComboBox1 = new JComboBox(); 
	 
	
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ArticleFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le nom de l'entreprise
	 */
	private JTextField textFieldType;

	/**
	 * zone de texte pour le numero de siret
	 */
	private JTextField textFieldDatedebut;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldDatefin;
	/**
	 * zone de texte pour le numero de telephone
	 */
	private JTextField textFieldNom_entreprise;
	
	private JLabel labelType;

	/**
	 * label  numero de siret
	 */
	private JLabel labelDate_debut;
	/**
	 * label  identifiant
	 */
	private JLabel labelDate_fin;

	/**
	 * label adresse
	 */
	private JLabel labelNom_entreprise;

	

	/**
	 * bouton d'ajout du client
	 */
	private JButton boutonCloturer_contrat;
	/**
	 * bouton d'ajout du client
	 */
	private JLabel labeluser;
	

	/**
	 * instance de clientDAO permettant les acc�s � la base de donn�es
	 */
	private contratDAO moncontratDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public contratfenetre() {
		// on instancie la classe client DAO
		this.moncontratDAO = new contratDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("Nouveau contrat");
		// initialisation de la taille de la fen�tre
		this.setSize(400, 400);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.WHITE);

		// instantiation des composants graphiques
		labeluser = new JLabel("Entrez les termes du contrat");
		textFieldType = new JTextField();
		textFieldDatedebut = new JTextField();
		textFieldDatefin = new JTextField();
		textFieldNom_entreprise = new JTextField();
		boutonCloturer_contrat = new JButton(" Confirmer la creation du contrat");
		
		labelNom_entreprise = new JLabel("Nom de l'entreprise :");
		labelType = new JLabel("Type des maintenances :");
		labelDate_debut = new JLabel("date de debut du contrat:");
		labelDate_fin = new JLabel("date de fin du contrat :");
		 ComboBox1.addItem("Corrective");
		 ComboBox1.addItem("Palliative");
		 ComboBox1.addItem("Preventive");
		 
		containerPanel.add(labeluser);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		// ajout des composants sur le container
		containerPanel.add(labelType);
		// introduire une espace constant entre le label et le champ texte
		
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(ComboBox1);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDate_debut);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldDatedebut);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDate_fin);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldDatefin);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNom_entreprise);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldNom_entreprise);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		

		containerPanel.add(boutonCloturer_contrat);

		containerPanel.add(Box.createRigidArea(new Dimension(20, 20)));

		

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonCloturer_contrat.addActionListener(this);
		

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe clientDAO
		 
		try {
			if (ae.getSource() == boutonCloturer_contrat) {
				
				
				// on cr�e l'objet message
				contrat_maintenance a = new contrat_maintenance( this.textFieldType.getText(),
						Date.valueOf(this.textFieldDatedebut.getText()),
						Date.valueOf(this.textFieldDatefin.getText()),
						this.textFieldNom_entreprise.getText());
			    
			
				// on demande � la classe de communication d'envoyer le client
				// dans la table CLIENT
				retour = moncontratDAO.ajouter(a);
				
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "un nouveau contrat a ete enregistr� !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout contrat",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} 
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler la saisie de vos donnees");
		}

	}

	public static void main(String[] args) {
		//dispose();
		new contratfenetre();
	}

}
