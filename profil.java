/**
 * Classe profil
 * @author grave - jeanine - antoine
 * @version 1.0
 * */
public class profil {
	/** 
	 * fonction d'un utilisateur
	 */
	private String fonction;
	/** 
	 * identifiant d'un profil
	 */
	private int identifiant;
	/**
	 * Constructor
	 * @param fonction
	 */
	public profil(String fonction, int identifiant){
		this.fonction=fonction;
		this.identifiant=identifiant;
	}
	public profil(String fonction){
		this.fonction=fonction;
	
	}
	/**
	 * getter pour la valeur de l'identifiant
	 * @return valeur de l'identifiant
	 */
	public int getIdentifiant(){
		return identifiant;
	}
	/**
	 * getter for attribute fonction
	 * @return value of fonction
	 */
	public String getFonction(){
		return fonction;
	}
	/**
	 * setter  for id
	 * @param puHt :  new value for attribute id
	 */
	public void setIdentifiant(int identifiant){
		this.identifiant=identifiant;
	}
	/**
	 * setter pour la fonction modifier une fonction
	 * @param puHt :  une nouvelle valeur est attribu� � la fonction
	 */
	public void setFonction(String fonction){
		this.fonction=fonction;
	}

	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "profil :  l'agent : " + fonction + " a ete ajoute comme nouveau agent de clientele et son identifiant est" + identifiant+"";
}
}

