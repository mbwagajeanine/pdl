import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table client
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public class ClientDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";  //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ClientDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(client client) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;
		
		


		// connexion à la base de données
		try {

			// tentative de connexion
			
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhait�
			ps = con.prepareStatement("INSERT INTO client (cl_nomentreprise, cl_codeape, cl_numsiret,cl_telephone, cl_id, cl_adresse ) VALUES (?, ?,?,?,?, ?)");
			ps.setString(1, client.getNomentreprise());
			ps.setString(2, client.getCodeApe());
			ps.setString(3, client.getNumerosiret());
			ps.setInt(4, Integer.parseInt(client.getTelephone()));
			ps.setInt(5, client.getIdentifiant());
			ps.setString(6, client.getAdresse());
			
			
			
			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un client à partir de son identifiant
	 * 
	 * @param reference
	 *            l'identifiant du client à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun client ne correspond à cette identifiant
	 */
	public client getclient(int identifiant) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		client retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client WHERE cl_identifiant = ?");
			ps.setInt(5, identifiant);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new client(rs.getString("cl_nomentremprise"),
						rs.getString("cl_codeape"),
						rs.getString("cl_numerodsiret"), rs.getString("cl_adresse"),
						rs.getString("cl_telephone"), rs.getInt("cl_identifiant")
						 );

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client
	 * 
	 * @return une ArrayList de clients
	 */
	public List<client> getListeclient() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<client> retour = new ArrayList<client>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new client(rs.getString("CL_NOMENTREPRISE"),
						rs.getString("CL_CODEAPE"),
						rs.getString("CL_NUMSIRET"), rs.getString("CL_ADRESSE"),
						rs.getString("CL_TELEPHONE"),
						rs.getInt("CL_ID")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ClientDAO ClientDAO = new ClientDAO();
		//client c1 = new client("esigelec","jfdjkk", "89214dd5", "fr76800","07698521",1);
		
		//int retour = ClientDAO.ajouter(c1);
		//System.out.println( +retour + "lignes ajoutées");
		

		// test de la méthode getclient
		//client a2 = ClientDAO.getclient(1);
		//System.out.println(a2);

		// test de la méthode getListeArticles
		List<client> liste = ClientDAO.getListeclient();
		// affichage des articles
		for (client cl : liste) {
			System.out.println(cl.toString());
		}

	}
}

