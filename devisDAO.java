
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table contrat maitenance
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public class devisDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public devisDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un contrat a une liste de contrat dans la table contrat Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param contrat
	 *            le contrat à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Devis devis) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO DEVIS (DEV_ID, DEV_PRIX, DEV_PRIXMAIN, DEV_MAIN_ID) VALUES (?, ?, ?,?)");
			ps.setInt(1, devis.getidentifiant());
			ps.setDouble(2, devis.getprixmateriaux());
			ps.setDouble(3, devis.getprixmaintenance());
			ps.setInt(4, devis.getmaintenance().getidMain());

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un article à partir de sa référence
	 * 
	 * @param reference
	 *            la référence de l'article à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun article ne correspond à cette référence
	 */
	public Devis getDevis(int identifiant) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Devis retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM DEVIS INNER JOIN MAINTENANCE ON DEVIS.DEV_MAIN_ID=MAINTENANCE.MAIN_ID  WHERE DEV_ID = ?");
			ps.setInt(1, identifiant);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Devis(rs.getInt("DEV_ID"),
						rs.getDouble("DEV_PRIX"),
						rs.getDouble("DEV_PRIXMAIN"), new maintenance(rs.getString("MAIN_STATUT"),rs.getDate("MAIN_DATEDEBUT"), rs.getString("MAIN_TYPE"), rs.getInt("MAIN_ID"), rs.getDate("MAIN_DATEFIN"), new client(rs.getString("CL_NOMENTREPRISE"),
								rs.getString("CL_CODEAPE"),
								rs.getString("CL_NUMSIRET"), rs.getString("CL_ADRESSE"),
								rs.getString("CL_TELEPHONE"),
								rs.getInt("CL_ID")), new operateur(rs.getString("OPT_NOM"),
						rs.getString("OPT_ADRESSE"),
						rs.getString("OPT_PRENOM"), rs.getInt("OPT_ID"), rs.getDate("OPT_DATE"))));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les articles stockés dans la table article
	 * 
	 * @return une ArrayList d'Articles
	 */
	public List<Devis> getListeDevis() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Devis> retour = new ArrayList<Devis>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM DEVIS INNER JOIN MAINTENANCE ON MAINTENANCE.MAIN_ID=DEVIS.DEV_MAIN_ID  WHERE DEV_ID =? ");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Devis(rs.getInt("DEV_ID"),
						rs.getDouble("DEV_PRIX"),
						rs.getDouble("DEV_PRIXMAIN"), new maintenance(rs.getString("MAIN_STATUT"),rs.getDate("MAIN_DATEDEBUT"), rs.getString("MAIN_TYPE"), rs.getInt("MAIN_ID"), rs.getDate("MAIN_DATEFIN"), new client(rs.getString("CL_NOMENTREPRISE"),
								rs.getString("CL_CODEAPE"),
								rs.getString("CL_NUMSIRET"), rs.getString("CL_ADRESSE"),
								rs.getString("CL_TELEPHONE"),
								rs.getInt("CL_ID")), new operateur(rs.getString("OPT_NOM"),
						rs.getString("OPT_ADRESSE"),
						rs.getString("OPT_PRENOM"), rs.getInt("OPT_ID"), rs.getDate("OPT_DATE")))));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		devisDAO devisDAO = new devisDAO();
		// test de la méthode ajouter
		client c1 = new client("ESIGELEC", "SS5585212222", "siret2548","5479922323","FR76800",2);
		operateur o1= new operateur("jeanine","mbwaga","fr76800",2,  Date.valueOf("2017-05-13"));
		maintenance m1 = new maintenance("non validee",Date.valueOf("2017-05-13"),"palliative",1, Date.valueOf("2018-05-26"), c1,o1);
		 Devis d1= new Devis(1, 1008.2, 599752.5,m1);
		int retour = devisDAO.ajouter(d1);

		System.out.println(retour + " lignes ajoutées");

		// test de la méthode getArticle
		Devis d2 = devisDAO.getDevis(1);
		System.out.println(d2);

		// test de la méthode getListeArticles
		List<Devis> liste = devisDAO.getListeDevis();
		// affichage des articles
		for (Devis dv : liste) {
			System.out.println(dv.toString());
		}

	}
}

