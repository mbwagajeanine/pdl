
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table utilisateur
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public class utilisateurDAO {

	public class verification {

	}

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public utilisateurDAO () {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un utilisateur dans la table utilisateur Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param article
	 *            l'utilisateur  à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(utilisateur uti) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO UTILISATEUR (LOGIN, UTI_MDP, UTI_ID, UTI_PR_ID) VALUES (?, ?, ?,?)");
			ps.setString(1, uti.getLogin());
			ps.setString(2, uti.getMdp() );
			ps.setInt(3, uti.getIdentifiant() );
			ps.setInt(4, uti.getProfil().getIdentifiant());
		

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un utilisateur à partir de son identifiant
	 * 
	 * @param identifiant
	 *            l' id de l'utilisateur à récupérer
	 * @return 	l'utilisateur trouvé;
	 * 			null si aucun utilisateur ne correspond à cet id
	 */
	public utilisateur getUtilisateur(String login) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		utilisateur retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM UTILISATEUR INNER JOIN PROFIL ON UTILISATEUR.UTI_ID = PROFIL.PR_ID WHERE UTI_FONCTION='?'");
			ps.setString(1, login);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new utilisateur (rs.getString("LOGIN"),
						rs.getString("UTI_MDP"),
						rs.getInt("UTI_ID"), new profil( rs.getString("PR_FONCTION"),
						rs.getInt("PR_ID")) );

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les utilisateurs
	 * 
	 * @return une ArrayList d'utilisateur
	 */
	public List<utilisateur> getListeutilisateur() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<utilisateur> retour = new ArrayList<utilisateur>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM UTILISATEUR INNER JOIN PROFIL ON UTILISATEUR.UTI_PR_ID=PROFIL.PR_ID");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new utilisateur( rs.getString("LOGIN"), rs.getString("UTI_MDP"), rs.getInt("UTI_ID"), new profil( rs.getString("PR_FONCTION"), rs.getInt("PR_ID"))) );

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		utilisateurDAO utilisateurDAO = new utilisateurDAO();
		// test de la méthode ajouter
		profil Profil = new profil("agent",1);
		utilisateur a1 = new utilisateur("jeanine" , "jeajea26",1 , Profil);
		int retour = utilisateurDAO.ajouter(a1);

		System.out.println(retour + " lignes ajoutées");

		// test de la méthode getutilisateur
		//utilisateur a2 = utilisateurDAO.getUtilisateur(1);
		//System.out.println(a2);

		// test de la méthode getListeutilisateur
		List<utilisateur> liste = utilisateurDAO.getListeutilisateur();
		// affichage des utilisateur
		for (utilisateur ctr : liste) {
			System.out.println(ctr.toString());
		}

	}
}
