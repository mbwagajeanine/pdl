import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table article
 * 
 * @author grave - roueche - serais
 * @version 1.2
 * */
public class maintenanceDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public maintenanceDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un article dans la table article Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param article
	 *            l'article à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(maintenance maintenance) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO MAINTENANCE (MAIN_STATUT, MAIN_DATEDEBUT, MAIN_TYPE, MAIN_ID,MAIN_DATEFIN, MAIN_CL_ID, MAIN_OPT_ID ) VALUES (?, ?, ?,?,?,?, ?)");
			ps.setString(1, maintenance.getStatut());
			ps.setDate(2, maintenance.getdatedebut() );
			ps.setString(3, maintenance.gettypeMaintenance() );
			ps.setInt(4, maintenance.getidMain() );
			ps.setDate(5, maintenance.getDatefin() );
			ps.setInt(6, maintenance.getClient().getIdentifiant() );
			ps.setInt(7, maintenance.getoperateur().getIdentifiant());
		
			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un article à partir de sa référence
	 * 
	 * @param reference
	 *            la référence de l'article à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun article ne correspond à cette référence
	 */
	public maintenance getmaintenance(int identifiant) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		maintenance retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM MAINTENANCE INNER JOIN CLIENT ON MAINTENANCE.MAIN_CL_ID= CLIENT.CL_ID INNER JOIN OPERATEUR ON MAINTENANCE.MAIN_OPT_ID=OPERATEUR.OPT_ID WHERE MAIN_ID=?");
			ps.setInt(4, identifiant);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new maintenance( rs.getString("MAIN_STATUT"),rs.getDate("MAIN_DATEDEBUT"),rs.getString("MAIN_TYPE"),rs.getInt("MAIN_ID"),rs.getDate("MAIN_DATEFIN"),
						new client(rs.getString("CL_NOMENTREPRISE"),
						rs.getString("CL_NUMEROSIRET"), rs.getString("CL_ADRESSE"),
						rs.getString("CL_CODEAPE"), rs.getString("CL_TELEPHONE"),
						rs.getInt("CL_ID")), new operateur(
								rs.getString("OPT_NOM"),
								rs.getString("OPT_ADRESSE"),
								rs.getString("OPT_PRENOM"), rs.getInt("OPT_ID"), rs.getDate("OPT_DATE")) );
			
		} catch (Exception ee) { 
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les articles stockés dans la table article
	 * 
	 * @return une ArrayList d'Articles
	 */
	public List<maintenance> getListemaintenance() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<maintenance> retour = new ArrayList<maintenance>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM MAINTENANCE INNER JOIN CLIENT ON MAINTENANCE.MAIN_CL_ID = CLIENT.CL_ID INNER JOIN OPERATEUR ON MAINTENANCE.MAIN_OPT_ID=OPERATEUR.OPT_ID");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new maintenance( rs.getString("MAIN_STATUT"),rs.getDate("MAIN_DATEDEBUT"),rs.getString("MAIN_TYPE"),rs.getInt("MAIN_ID"),rs.getDate("MAIN_DATEFIN"),
						new client(rs.getString("CL_NOMENTREPRISE"),
						rs.getString("CL_NUMSIRET"), rs.getString("CL_ADRESSE"),
						rs.getString("CL_CODEAPE"), rs.getString("CL_TELEPHONE"),
						rs.getInt("CL_ID")), new operateur(
								rs.getString("OPT_NOM"),
								rs.getString("OPT_ADRESSE"),
								rs.getString("OPT_PRENOM"), rs.getInt("OPT_ID"), rs.getDate("OPT_DATE"))));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		maintenanceDAO maintenanceDAO = new maintenanceDAO();
		// test de la méthode ajouter
		
		//client c1 = new client("esigelec","jfdjkk", "89214dd5", "fr76800","07698521",42);
		//operateur o1 = new operateur("jeanine","mbwaga","fr76800",1,  Date.valueOf("2017-05-13") );
		//maintenance m1= new maintenance("validee",Date.valueOf("2016-05-18"), "palliative",1,Date.valueOf("2016-06-01"),c1,o1);
		//int retour = maintenanceDAO.ajouter(m1);

		//System.out.println(retour + " lignes ajoutées");

		// test de la méthode getArticle
		//Article a2 = articleDAO.getArticle(1);
		//System.out.println(a2);

		// test de la méthode getListeArticles
		List<maintenance> liste = maintenanceDAO.getListemaintenance();
		// affichage des articles
		for (maintenance main : liste) {
			System.out.println(main.toString());
		}

	}
}

