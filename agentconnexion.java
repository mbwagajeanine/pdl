import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import utilisateurDAO.verification;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.SwingConstants;

public class agentconnexion extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldlogin;
	private JPasswordField passwordField;
	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					agentconnexion frame = new agentconnexion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public agentconnexion() {
		setTitle("Gestion de Maintenances");
		getContentPane().setLayout(null);
		getContentPane().setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblNewLabel = new JLabel("BIENVENUE/WELCOME");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblNewLabel.setBounds(107, 12, 214, 42);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Veuillez saisir vos identifiants");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel_1.setBounds(88, 65, 229, 14);
		contentPane.add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("connexion");
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton.setBounds(168, 187, 124, 33);
		contentPane.add(btnNewButton);
		
		JLabel login = new JLabel("Login");
		login.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		login.setBounds(86, 104, 46, 14);
		contentPane.add(login);
		//textFieldid = new JTextField();
		//textFieldmdp = new JTextField();
		
		textFieldlogin = new JTextField();
		textFieldlogin.setBounds(171, 101, 86, 20);
		contentPane.add(textFieldlogin);
		textFieldlogin.setColumns(10);
		
		JLabel motdp = new JLabel("Mot de passe");
		motdp.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		motdp.setBounds(57, 138, 92, 14);
		contentPane.add(motdp);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(170, 135, 87, 20);
		contentPane.add(passwordField);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String Login = textFieldlogin.getText();
					String mdp = passwordField.getText();
					if( verification.getverification( Login, mdp ).equals("agent")){
						Menu frame = new Menu();
						frame.setVisible(true);
					}
					else if(verification.getverification(Login, mdp ).equals("administrateur")){
						menuadministrateur frame = new menuadministrateur();
						frame.setVisible(true);
					}
				} catch (Exception ae) {
					ae.printStackTrace();
					JOptionPane.showMessageDialog( contentPane,
							"Vos identifiants sont incorrectes", "Erreur",
							JOptionPane.ERROR_MESSAGE);
				}

				
				dispose();
		
	}
});
	}
}

