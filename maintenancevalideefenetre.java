

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;
import java.sql.*;


import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 * Classe clientfenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client via
 * la saisie du nom de l'entreprise,le numero de siret, le numero de telephone l'adresse 
 *    - Permet l'affichage de tous les clients dans la console
 * @author grave - jeanine-antoine
 * @version 1.3
 * */


public class maintenancevalideefenetre extends JFrame implements ActionListener {
	
	 
	 // JComboBox ComboBox1 = new JComboBox(); 
	  JComboBox ComboBox = new JComboBox(); 
	
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ArticleFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le 
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour le 
	 */
	private JTextField textFieldtype;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldstatut;
	
	/**
	 * zone de texte pour 
	 */
	private JTextField textFielddatedebut;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFielddatefin;
	/**
	 * zone du client
	 */
	/**
	 * zone de texte pour le nom de l'entreprise
	 */
	private JTextField textFieldNomEntreprise;

	/**
	 * zone de texte pour le numero de siret
	 */
	private JTextField textFieldNumeroSiret;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldAdresse;
	/**
	 * zone de texte pour le numero de telephone
	 */
	private JTextField textFieldNumeroTelephone;
	/**
	 * zone de texte pour l'identifiant
	 */
	private JTextField textFieldIdentifiantclient;
	/**
	 * zone de texte pour le code ape
	 */
	private JTextField textFieldcodeApe;
	
	
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldNom;

	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldPrenom;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldAdresseoperateur;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldDateembauche;
	/**
	 * zone de texte pour l'identifiant
	 */
	/**
	 * bouton d'ajout de la maintenance
	 */
	private JButton boutonvalidermaintenance;
	/**
	 * bouton 
	 */
	private JButton boutonDevis;
	/**
	 * bouton 
	 */
	private JButton boutonoperateur;
	/**
	 * bouton 
	 */
	private JButton boutonRetour;
	/**
	 * bouton 
	 */
	private JButton boutonaffichertouteslesmaintenances;
	
	/**
	 * bouton 
	 */
	private JLabel labeluser;
	/**
	 * bouton 
	 */
	private JLabel labelstatut;
	/**
	 * Zone de texte pour afficher les articles
	 */
	JTextArea zoneTextListmaintenance;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de clientDAO permettant les acc�s � la base de donn�es
	 */
	private maintenanceDAO maintenanceDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public maintenancevalideefenetre() {
		// on instancie la classe client DAO
		this.maintenanceDAO = new maintenanceDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("Validation d'une maintenance");
		// initialisation de la taille de la fen�tre
		this.setSize(500, 500);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.WHITE);

		// instantiation des composants graphiques
		labeluser = new JLabel("Verifiez les informations sur cette maintenance, definir le devis et affecter un operateur");
		textFieldtype = new JTextField();
		textFieldstatut = new JTextField();
		textFieldIdentifiant= new JTextField();
		textFielddatedebut = new JTextField();
		textFielddatefin = new JTextField();
		textFieldNom = new JTextField();
		textFieldPrenom = new JTextField();
		textFieldAdresseoperateur = new JTextField();
		textFieldDateembauche = new JTextField();
	
		textFieldNomEntreprise = new JTextField();
		textFieldNumeroSiret = new JTextField();
		textFieldAdresse = new JTextField();
		textFieldNumeroTelephone = new JTextField();
		textFieldcodeApe = new JTextField();
		
		boutonvalidermaintenance = new JButton(" valider une maintenance");
		boutonDevis=new JButton("Cr�er le Devis");
		boutonRetour= new JButton("Retour");
		boutonoperateur=new JButton("affecter un operateur");
		boutonaffichertouteslesmaintenances= new JButton("afficher les statuts des maintenances");
		
	
		labelstatut = new JLabel("Statut :");
		ComboBox.addItem("validee");
		 
		 zoneTextListmaintenance = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListmaintenance);
		zoneTextListmaintenance.setEditable(false);
		
		containerPanel.add(labeluser);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 10)));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		containerPanel.add(boutonaffichertouteslesmaintenances);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 5)));
		containerPanel.add(zoneDefilement);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));
		
		
		// ajout des composants sur le container
		
		//containerPanel.add(labelstaut);
		// introduire une espace constant entre le label et le champ texte
		
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		//containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		//containerPanel.add(ComboBox1);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelstatut);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(ComboBox);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(boutonDevis);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));
		containerPanel.add(boutonoperateur);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));

		containerPanel.add(boutonvalidermaintenance);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));
		containerPanel.add(boutonRetour);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 5)));
		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonvalidermaintenance.addActionListener(this);
		boutonRetour.addActionListener(this);
		boutonDevis.addActionListener(this);
		boutonoperateur.addActionListener(this);
		boutonaffichertouteslesmaintenances.addActionListener(this);
		
		

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe clientDAO
		 
		try {
			if (ae.getSource() == boutonvalidermaintenance) {
				
				
				// on cr�e l'objet message
				maintenance a = new maintenance(this.textFieldstatut.getText(),
						Date.valueOf(this.textFielddatedebut.getText()),
						this.textFieldtype.getText(), Integer.parseInt(this.textFieldIdentifiant.getText()),
						Date.valueOf(this.textFielddatefin.getText()),
						new client(
								this.textFieldNomEntreprise.getText(),
								this.textFieldNumeroSiret.getText(),
								this.textFieldAdresse.getText(),
								this.textFieldcodeApe.getText(),
								this.textFieldNumeroTelephone.getText()), new operateur(this.textFieldNom.getText(),
										this.textFieldPrenom.getText(),
										this.textFieldAdresse.getText(),
										Date.valueOf(this.textFieldDateembauche.getText()))
						
						);
			    
			
				// on demande � la classe de communication d'envoyer le client
				// dans la table CLIENT
				retour = maintenanceDAO.ajouter(a);
				
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "un maintenance a ete validee !");
				else
					JOptionPane.showMessageDialog(this, "erreur validation maintenance",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonaffichertouteslesmaintenances) {
				// on demande � la classe ArticleDAO d'ajouter le message
				// dans la base de donn�es
				List<maintenance> liste = maintenanceDAO.getListemaintenance();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListmaintenance.setText("");
				// on affiche dans la console du client
				for (maintenance a : liste) {
					zoneTextListmaintenance.append(a.toString());
					zoneTextListmaintenance.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			}
			else if(ae.getSource() == boutonDevis){
				
				new devisfenetre();
				dispose();
			}
			else if (ae.getSource() == boutonRetour){
				menuadministrateur frame = new menuadministrateur();
				frame.setVisible(true);
				dispose();
			}
			else if(ae.getSource() == boutonoperateur){
				
				new operateurfenetre();
				dispose();
			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler la saisie de vos donnees");
		}

	}

	public static void main(String[] args) {
		//dispose();
		new maintenancevalideefenetre();
	}

}

