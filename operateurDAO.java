
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table operateur
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public class operateurDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";  //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public operateurDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(operateur operateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;
		
		


		// connexion à la base de données
		try {

			// tentative de connexion
			
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhait�
			ps = con.prepareStatement("INSERT INTO OPERATEUR (OPT_ID, OPT_NOM, OPT_PRENOM, OPT_DATE, OPT_ADRESSE, OPT_DISPO ) VALUES (?, ?,?,?, ?, ?)");
			ps.setInt(1, operateur.getIdentifiant() );
			ps.setString(2, operateur.getnom());
			ps.setString(3, operateur.getPrenom() );
			ps.setDate(4, operateur.getdateoperateur());
			ps.setString(5, operateur.getAdresse());
			ps.setBoolean(6, operateur.getdisponibilite());
			
			
			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un client à partir de son identifiant
	 * 
	 * @param reference
	 *            l'identifiant du client à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun client ne correspond à cette identifiant
	 */
	public operateur getoperateur(int identifiant) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		operateur retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM OPERATEUR WHERE OPT_ID= ?");
			ps.setInt(1, identifiant);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new operateur(
						rs.getString("OPT_NOM"),
						rs.getString("OPT_ADRESSE"),
						rs.getString("OPT_PRENOM"), rs.getInt("OPT_ID"), rs.getDate("OPT_DATE"),rs.getBoolean("OPT_DISPO"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client
	 * 
	 * @return une ArrayList de clients
	 */
	public List<operateur> getListeoperateur() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<operateur> retour = new ArrayList<operateur>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM OPERATEUR");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new operateur(
						rs.getString("OPT_NOM"),
						rs.getString("OPT_ADRESSE"),
						rs.getString("OPT_PRENOM"), rs.getInt("OPT_ID"), rs.getDate("OPT_DATE"), rs.getBoolean("OPT_dispo")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		operateurDAO operateurDAO = new operateurDAO();
		int retour=0;
		// test de la méthode ajouter
		//operateur a1 = new operateur("jeanine","mbwaga","fr76800",1, Date.valueOf("2017-05-13"),0 );
		//retour = operateurDAO.ajouter(a1);
		//System.out.println( +retour + "lignes ajoutées");

		// test de la méthode getclient
		//operateur a2 = operateurDAO.getoperateur();
		//System.out.println(a2);

		// test de la méthode getListeArticles
		List<operateur> liste = operateurDAO.getListeoperateur();
		// affichage des articles
		for (operateur opt : liste) {
			System.out.println(opt.toString());
		}

	}
}


