
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe clientfenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client via
 * la saisie du nom de l'entreprise,le numero de siret, le numero de telephone l'adresse 
 *    - Permet l'affichage de tous les clients dans la console
 * @author grave - jeanine-antoine
 * @version 1.3
 * */


public class clientfenetre extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ArticleFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le nom de l'entreprise
	 */
	private JTextField textFieldNomEntreprise;

	/**
	 * zone de texte pour le numero de siret
	 */
	private JTextField textFieldNumeroSiret;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldAdresse;
	/**
	 * zone de texte pour le numero de telephone
	 */
	private JTextField textFieldNumeroTelephone;
	/**
	 * zone de texte pour l'identifiant
	 */
	private JTextField textFieldIdentifiant;
	/**
	 * zone de texte pour le code ape
	 */
	private JTextField textFieldcodeApe;
	/**
	 * label nomentreprise
	 */
	private JLabel labelNomEntreprise;

	/**
	 * label  numero de siret
	 */
	private JLabel labelNumeroSiret;
	/**
	 * label  identifiant
	 */
	private JLabel labelIdentifiant;

	/**
	 * label adresse
	 */
	private JLabel labelAdresse;

	/**
	 * label numero de telephone
	 */
	private JLabel labelNumeroTelephone;
	/**
	 * label code APE
	 */
	private JLabel labelCodeApe;

	/**
	 * bouton d'ajout du client
	 */
	private JButton boutonAjouter_Un_Nouveau_Client;
	/**
	 * bouton d'ajout du client
	 */
	private JButton boutonretour;
	/**
	 * bouton d'ajout du client
	 */
	private JLabel labeluser;
	/**
	 * bouton qui permet d'afficher tous les clients
	 */
	private JButton boutonAffichage_de_Tous_Les_Clients;

	/**
	 * Zone de texte pour afficher les articles
	 */
	JTextArea zoneTextListclient;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de clientDAO permettant les acc�s � la base de donn�es
	 */
	private ClientDAO monclientDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public clientfenetre() {
		// on instancie la classe client DAO
		this.monclientDAO = new ClientDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("Nouveau Client");
		// initialisation de la taille de la fen�tre
		this.setSize(600, 600);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.WHITE);

		// instantiation des composants graphiques
		labeluser = new JLabel("veuillez entrez un nouveau client");
		textFieldNomEntreprise = new JTextField();
		textFieldNumeroSiret = new JTextField();
		textFieldAdresse = new JTextField();
		textFieldNumeroTelephone = new JTextField();
		textFieldcodeApe = new JTextField();
		boutonAjouter_Un_Nouveau_Client = new JButton("ajouter un nouveau client");
		boutonAffichage_de_Tous_Les_Clients = new JButton(
				"afficher tous les clients");
		boutonretour=new JButton("Retour");
		labelNomEntreprise = new JLabel("Nom de l'entreprise :");
		labelNumeroSiret = new JLabel("Numero de SIRET :");
		labelAdresse = new JLabel("ADRESSE :");
		labelNumeroTelephone = new JLabel("numero de telephone :");
		labelCodeApe = new JLabel("code APE :");

		zoneTextListclient = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListclient);
		zoneTextListclient.setEditable(false);

		containerPanel.add(labeluser);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		// ajout des composants sur le container
		containerPanel.add(labelNomEntreprise);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNomEntreprise);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNumeroSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldNumeroSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelAdresse);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldAdresse);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNumeroTelephone);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldNumeroTelephone);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(labelCodeApe);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldcodeApe);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(boutonAjouter_Un_Nouveau_Client);

		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));

		containerPanel.add(boutonAffichage_de_Tous_Les_Clients);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 5)));
		containerPanel.add(zoneDefilement);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));
		containerPanel.add(boutonretour);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 5)));
		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonAjouter_Un_Nouveau_Client.addActionListener(this);
		boutonAffichage_de_Tous_Les_Clients.addActionListener(this);
		boutonretour.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe clientDAO

		try {
			if (ae.getSource() == boutonAjouter_Un_Nouveau_Client) {
				
				
				// on cr�e l'objet message
				client a = new client( this.textFieldNomEntreprise.getText(),
						this.textFieldNumeroSiret.getText(),
						this.textFieldAdresse.getText(),
						this.textFieldcodeApe.getText(),
						this.textFieldNumeroTelephone.getText());
				
				// on demande � la classe de communication d'envoyer le client
				// dans la table CLIENT
				retour = monclientDAO.ajouter(a);
				
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "un nouveau client ajout� ! verifier le dans la liste des clients");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout client",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichage_de_Tous_Les_Clients) {
				// on demande � la classe ArticleDAO d'ajouter le message
				// dans la base de donn�es
				List<client> liste = monclientDAO.getListeclient();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListclient.setText("");
				// on affiche dans la console du client
				for (client a : liste) {
					zoneTextListclient.append(a.toString());
					zoneTextListclient.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			}
			else if(ae.getSource()== boutonretour) {
				Menu frame = new Menu();
				frame.setVisible(true);
				dispose();
			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler la saisie de vos donnees");
		}

	}

	public static void main(String[] args) {
		//dispose();
		new clientfenetre();
	}

}
