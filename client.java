/**
 * Classe customer
 * @author grave - jeanine - antoine
 * @version 1.0
 * */
public class client {
	/** 
	 * name of the enterprise;
	 */
	private String nomentreprise;
	/** 
	 * SIRET number
	 */
	private String numerosiret;
	/** 
	 * customer adress
	 */
	private String adresse;
	/** 
	 * customer APE cod 
	 */
	private String codeApe;
	/** 
	 * customer cellphone
	 */
	private String telephone;
	/** 
	 * id of the customer
	 */
	private int identifiant;
	
	/**
	 * Constructor
	 * @param adresse du client
	 * @param code APE du client
	 * @param identifiant du client
	 * @param numero du siret du client
	 * @param numero de siret
	 * @param telephone du client
	 */
	public client (String nomentreprise, String numerosiret, String adresse, String codeApe,String telephone, int identifiant){
		this.adresse=adresse;
		this.codeApe=codeApe;
		this.identifiant=identifiant;
		this.nomentreprise=nomentreprise;
		this.numerosiret=numerosiret;
		this.telephone=telephone;
	}
	
	public client (String nomentreprise, String numerosiret, String adresse, String codeApe,String telephone){
		this.adresse=adresse;
		this.codeApe=codeApe;
		this.nomentreprise=nomentreprise;
		this.numerosiret=numerosiret;
		this.telephone=telephone;
	}
	public client (String nomentreprise){
		
		this.nomentreprise=nomentreprise;
		
	}
	/**
	 * getter pour l'adresse du client
	 * @return valeur de l'adresse
	 */
	public String getAdresse(){
		return adresse;
	}
	/**
	 * getter pour le code APE du client
	 * @return valeur du code
	 */
	public String getCodeApe(){
		return codeApe;
	}
	/**
	 * getter pour l''identifiant du client
	 * @return valeur de l'id
	 */
	public int getIdentifiant(){
		return identifiant;
	}
	/**
	 * getter pour le nom de l'enreprise
	 * @return valeur du nom de l'entreprise
	 */
	public String getNomentreprise(){
		return nomentreprise;
	}
	/**
	 * getter pour le numero de siret du client
	 * @return valeur du numero de siret
	 */
	public String getNumerosiret(){
		return numerosiret;
	}
	/**
	 * getter pour le numero de telephone du client
	 * @return valeur du numero
	 */
	public String getTelephone(){
		return telephone;
	}
	
	/**
	 * setter pour le numero de telephone
	 * @param idMain :  une nouvelle valeur est attribu� au numero de telephone
	 */
	public void setTelephone(String telephone){
		this.telephone=telephone;
	}
	/**
	 * setter pour le nom de l'entreprise
	 * @param nomentreprise : valeur du nom de l'entreprise 
	 */
	public void setNomentreprise(String nomentreprise){
		this.nomentreprise=nomentreprise;
	}
	/**
	 * setter pour l'adresse
	 * @param adresse :  une nouvelle valeur est attribu� a l'adresse
	 */
	public void setAdresse(String adresse){
		this.adresse=adresse;
	}
	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage du client
	 */
	public String toString() {
		return "client [nom de l'entreprise : " + nomentreprise + " - adresse: " + adresse
				+ "-numero de siret : " + numerosiret + " -numero de telephone: "+ telephone +"]";
	}
	
}
