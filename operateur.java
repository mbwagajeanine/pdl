/**
 * Classe customer
 * @author grave - jeanine - antoine
 * @version 1.0
 * */
public class operateur {
	/** 
	 * nom de l'operateur
	 */
	private String nom;
	/** 
	 * prenom de l'operateur
	 */
	private String prenom;
	/** 
	 * adresse de l'operateur
	 */
	private String adresse;
	/** 
	 * identifiant de l'operateur
	 */
	private int identifiant;
	
	/** 
	 * date d'embauche
	 */
	private java.sql.Date dateoperateur;
	/** 
	 * date d'embauche
	 */
	private boolean disponibilite; 
	/**
	 * Constructor
	 * @param nom de l'operateur
	 * @param prenom de l'operateur
	 * @param adresse de l'operateur
	 * @param identifiant de l'operateur
	 * @param date d'embauche de l'operateur
	 */
	public operateur (String nom, String prenom, String adresse, int identifiant, java.sql.Date dateoperateur, boolean disponibilte){
		this.adresse=adresse;
		this.nom=nom;
		this.identifiant=identifiant;
		this.dateoperateur=dateoperateur;
		this.prenom=prenom;
		this.disponibilite=disponibilte;
	}
	public operateur (String nom, String prenom, String adresse, java.sql.Date dateoperateur, boolean disponibilite){
		this.adresse=adresse;
		this.nom=nom;
		this.dateoperateur=dateoperateur;
		this.prenom=prenom;
		this.disponibilite=disponibilite;
	}
	
	
	/**
	 * getter pour l'adresse du client
	 * @return valeur de l'adresse
	 */
	public String getAdresse(){
		return adresse;
	}
	/**
	 * getter 
	 * @return
	 */
	public String getnom(){
		return nom;
	}
	/**
	 * getter 
	 * @return 
	 */
	public int getIdentifiant(){
		return identifiant;
	}
	/**
	 * getter 
	 * @return 
	 */
	public String getPrenom(){
		return prenom;
	}

	/**
	 * getter pour le numero de telephone du client
	 * @return valeur du numero
	 */
	public java.sql.Date getdateoperateur(){
		return dateoperateur;
	}
	/**
	 * getter 
	 * @return 
	 */
	public boolean getdisponibilite(){
		return disponibilite;
	}

	
	/**
	 * setter pour le numero de telephone
	 * @param idMain :  une nouvelle valeur est attribu� au numero de telephone
	 */
	public void setNom(String nom){
		this.nom=nom;
	}
	/**
	 * setter pour le nom de l'entreprise
	 * @param nomentreprise : valeur du nom de l'entreprise 
	 */
	public void setprenom(String prenom){
		this.prenom=prenom;
	}
	/**
	 * setter pour l'adresse
	 * @param adresse :  une nouvelle valeur est attribu� a l'adresse
	 */
	public void setAdresse(String adresse){
		this.adresse=adresse;
	}
	/**
	 * setter pour le nom de l'entreprise
	 * @param nomentreprise : valeur du nom de l'entreprise 
	 */
	public void setdisponibilite(boolean disponibilite){
		this.disponibilite=disponibilite;
	}
	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage du client
	 */
	public String toString() {
		return "l'op�rateur"  + nom   +prenom + "  localise  a l'adresse: " +adresse+ " a ete embauche le : " +dateoperateur+ " et est disponible pour une maintenance" +disponibilite;
	}
	
}
