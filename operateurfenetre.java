

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe clientfenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client via
 * la saisie du nom de l'entreprise,le numero de siret, le numero de telephone l'adresse 
 *    - Permet l'affichage de tous les clients dans la console
 * @author grave - jeanine-antoine
 * @version 1.3
 * */


public class operateurfenetre extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ArticleFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldNom;

	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldPrenom;

	/**
	 * zone de texte pour l'adresse
	 * 
	 */
	private JTextField textFieldAdresse;
	/**
	 * zone de texte pour 
	 */
	private JTextField textFieldDateembauche;
	/**
	 * zone de texte pour l'identifiant
	 */
	private JTextField textFieldIdentifiant;
	
	/**
	 * label nom
	 */
	private JLabel labelNom;

	/**
	 * label 
	 */
	private JLabel labelPrenom;
	/**
	 * label  identifiant
	 */
	private JLabel labelIdentifiant;

	/**
	 * label adresse
	 */
	private JLabel labelAdresse;

	/**
	 * label numero de telephone
	 */
	private JLabel labelDateembauche;
	

	/**
	 * bouton d'ajout du client
	 */
	private JButton boutonAjouternouveloperateur;
	/**
	 * bouton d'ajout du client
	 */
	private JLabel labeluser;
	/**
	 * bouton qui permet d'afficher tous les clients
	 */
	private JButton boutonAffichagedetouslesoperateurs;

	/**
	 * Zone de texte pour afficher les articles
	 */
	JTextArea zoneTextListoperateur;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de clientDAO permettant les acc�s � la base de donn�es
	 */
	private operateurDAO monoperateurDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public operateurfenetre() {
		// on instancie la classe client DAO
		this.monoperateurDAO = new operateurDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("Nouvel operateur");
		// initialisation de la taille de la fen�tre
		this.setSize(500, 500);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.WHITE);

		// instantiation des composants graphiques
		labeluser = new JLabel("Veuillez entrez les informations sur le nouvel operateur");
		textFieldNom = new JTextField();
		textFieldPrenom = new JTextField();
		textFieldAdresse = new JTextField();
		textFieldDateembauche = new JTextField();
		textFieldIdentifiant = new JTextField();
		boutonAjouternouveloperateur = new JButton("Ajouter un nouveau operateur");
		boutonAffichagedetouslesoperateurs = new JButton(
				"Afficher tous les operateurs");
		labelNom = new JLabel("Nom de l'operateur :");
		labelPrenom = new JLabel("Prenom de l'operateur :");
		labelAdresse = new JLabel("ADRESSE de l'operateur :");
		labelDateembauche = new JLabel("Date d'embauche :");

		zoneTextListoperateur = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListoperateur);
		zoneTextListoperateur.setEditable(false);

		containerPanel.add(labeluser);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		// ajout des composants sur le container
		containerPanel.add(labelNom);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNom);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelPrenom);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldPrenom);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelAdresse);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldAdresse);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDateembauche);
		containerPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		containerPanel.add(textFieldDateembauche);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
	

		containerPanel.add(boutonAjouternouveloperateur);

		containerPanel.add(Box.createRigidArea(new Dimension(30, 20)));

		containerPanel.add(boutonAffichagedetouslesoperateurs);
		containerPanel.add(Box.createRigidArea(new Dimension(10, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonAjouternouveloperateur.addActionListener(this);
		boutonAffichagedetouslesoperateurs.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe clientDAO

		try {
			if (ae.getSource() == boutonAjouternouveloperateur) {
				
				
				// on cr�e l'objet message
				operateur a = new operateur( this.textFieldNom.getText(),
						this.textFieldPrenom.getText(),
						this.textFieldAdresse.getText(),
						Date.valueOf(this.textFieldDateembauche.getText()),Boolean.parseBoolean(this)
						);
				
				// on demande � la classe de communication d'envoyer le client
				// dans la table CLIENT
				retour = monoperateurDAO.ajouter(a);
				
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "un nouvel operateur a �t� ajout� ! verifier le dans la liste des operateurs");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout op�rateur",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichagedetouslesoperateurs) {
				// on demande � la classe ArticleDAO d'ajouter le message
				// dans la base de donn�es
				List<operateur> liste = monoperateurDAO.getListeoperateur();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListoperateur.setText("");
				// on affiche dans la console du client
				for (operateur op : liste) {
					zoneTextListoperateur.append(op.toString());
					zoneTextListoperateur.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler la saisie de vos donnees");
		}

	}

	public static void main(String[] args) {
		//dispose();
		new operateurfenetre();
		
	}

}

