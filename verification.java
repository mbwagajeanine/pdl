

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table contrat maitenance
 * 
 * @author jeanine-antoine
 * @version 1.2
 * */
public final class verification {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "mbwagangono2698";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public verification() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	

	/**
	 * Permet de récupérer un article à partir de sa référence
	 * 
	 * @param reference
	 *            la référence de l'article à récupérer
	 * @return 	l'article trouvé;
	 * 			null si aucun article ne correspond à cette référence
	 */
	public static String getverification(String login, String mdp) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String fonction = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT PR_FONCTION FROM PROFIL INNER JOIN  UTILISATEUR ON PROFIL.PR_ID=UTILISATEUR.UTI_PR_ID WHERE  UTILISATEUR.LOGIN=? AND UTILISATEUR.UTI_MDP=?");
			ps.setString(1, login);
			ps.setString(2, mdp);
			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				fonction = (rs.getString("PR_FONCTION"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return fonction;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		verification verification = new verification();
		String fonction = verification.getverification("jeanine", "jeajea26");
		System.out.println(fonction);
		
		}

	}



